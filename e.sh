#!/bin/bash

function up {
    export SAND_ALIAS=$1
    docker-compose -p att_$SAND_ALIAS -f docker/docker-compose.sandbox.yml up -d
}
function down {
    export SAND_ALIAS=$1
    docker-compose -p att_$SAND_ALIAS -f docker/docker-compose.sandbox.yml down
}

function clone {
    if [[ ! -d btr/$1 ]]; then
        echo source folder not exist
        exit 1
    elif [[ -d btr/$2 ]]; then
        echo folder exist
        exit 1
    else
        btrfs subvolume snapshot btr/$1 btr/$2
    fi 
}
function delete {
    btrfs subvolume delete btr/$1 
}
function create {
    if [[ -d btr/$1 ]]; then
        echo folder exist
        exit 1
    else
        btrfs subvolume create btr/$1 
        mkdir btr/$1/www
        mkdir btr/$1/db
    fi 
}

# заупустить apache и mysql
if [[ $1 == "up" && "$2" ]]; then
    up $2
# остановить apache и mysql
elif  [[ $1 == "down" && "$2" ]]; then
    down $2 

# сделать клон сабволюма
elif  [[ $1 == "clone" && "$2" && "$3" ]]; then
    clone $2 $3
# удалить сабволюм
elif  [[ $1 == "delete" && "$2" ]]; then
    delete $2 
# создать пустой сабволюм
elif  [[ $1 == "create" && "$2" ]]; then
    create $2

# создать и запустить пустой песок
elif  [[ $1 == "init" && "$2" ]]; then
    create $2 && up $2
# сделать клон от base и запустить песок
elif  [[ $1 == "start" && "$2" ]]; then
    clone base $2 && up $2
# остановить и очистить песок
elif  [[ $1 == "stop" && "$2" ]]; then
    down $2 && delete $2
# сбросить песок до состояния base
elif  [[ $1 == "reset" && "$2" ]]; then
    down $2 && delete $2 
    clone base $2 && up $2
else
    echo no action
fi





